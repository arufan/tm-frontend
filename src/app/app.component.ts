
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationStart, Router, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';


declare var AdminLTE: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {
  title = 'app';
  sub: any;

  bodyClasses = 'skin-blue sidebar-mini fixed';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];

  constructor(private slimLoadingBarService: SlimLoadingBarService,
    private router: Router
  ){
   console.log('app starting...')

  }
  ngOnInit() {
    

  
  }

  ngOnDestroy() {
    // remove the the body classes
  
  }


}
