
import { CanActivate } from '@angular/router';
import { ControlValueAccessor } from '@angular/forms';
 
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';



@NgModule({
  imports: [
    RouterModule.forRoot([
       
      { path: '',  component: HomeComponent},
      
      
          
    ], {useHash: false})
  ],
  declarations: [],
  exports: [ RouterModule]
})
export class AppRoutingModule { }
