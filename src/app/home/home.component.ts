import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { OrderService } from '../services/order.service';
import { ModalService } from '../services/modal.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit  {

  dtTrigger: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  company_disabled:string="";
  

  dtLangPaginateSetting:DataTables.LanguagePaginateSettings={
    next:'>>',
    previous: '<<'
    
  } as any;
  dtLangSetting:DataTables.LanguageSettings={
    paginate:this.dtLangPaginateSetting,
    lengthMenu:'',
    searchPlaceholder: 'Search',
    search: '',
    info: '',
    infoEmpty: '',
    infoFiltered: '',
    infoPostFix: ''
  };

  dtOptions: DataTables.Settings = {
   
    language:this.dtLangSetting
     
  };
   

  orders:any=[];
  

  constructor(
    private orderService:OrderService,
    private modalService:ModalService
    ) {
      
      this.orderService.refreshList$.subscribe(()=>{
          this.loadData();
          
      })

     }

  ngOnInit() {

    this.loadData();
    

  }

  rerender(): void {
    console.log(this.dtElement);
    if(this.dtElement && this.dtElement.dtInstance){
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      });
    }else{
      this.dtTrigger.next();
    }
    
  }

  loadData(){
    this.orderService.getAll().subscribe(res=>{
      this.orders=res;
      this.rerender();
    })
  }

  addOrder(){
    this.modalService.openModel(
      'view',{
        data:{},
        mode: 'add'
      }
    );
  }

  viewOrder(order:any){
    this.modalService.openModel(
      'view',{
        data:order,
        mode: 'edit'
      }
    );
  }

   

}
