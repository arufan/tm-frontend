import { Injectable } from '@angular/core';

@Injectable()
export class DatatableHelper {
    public tableWidget: any;
    selector:any;
    public initDatatable(selector:any): void {
        let exampleId: any = $(selector);
        this.selector=selector;
        this.tableWidget = exampleId.DataTable({
          select: true,
          "processing": true
        });
      }
      
    
      public reInitDatatable(): void {
        
        if (this.tableWidget) {
          this.tableWidget.destroy()
          this.tableWidget=null
        }
        setTimeout(() => this.initDatatable(this.selector),0)
      }

      public destroyDataTable(): void {
        
        if (this.tableWidget) {
          this.tableWidget.destroy()
          this.tableWidget=null
        }
      }
}
