
import { TokenInterceptor } from './auth/token-interceptor';
import { DatatableHelper } from './utils/datatable-helper';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, InjectionToken } from '@angular/core';

import { AppComponent } from './app.component';

import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';

import { ChartsModule } from 'ng2-charts';

//added by arfan for scrollable div on click
import { Ng2ScrollableModule } from 'ng2-scrollable';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import {HttpClientModule} from '@angular/common/http';
import { ImageUploadModule } from "angular2-image-upload";
import { DatepickerModule } from 'angular2-material-datepicker'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { ClearimageDirective } from './directives/clearimage.directive';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { TextMaskModule } from 'angular2-text-mask';
import { InputCounterComponent } from './libs/input-counter/input-counter.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

import { QuillEditorModule } from 'ngx-quill-editor';
import { EditableDirective } from './directives/editable.directive';



import { ReplaceUnderscorePipe } from './pipes/replace-underscore.pipe';
import { TreeviewModule } from 'ngx-treeview';
import { AmazingTimePickerModule } from 'amazing-time-picker';


import { FullCalendarModule } from 'ng-fullcalendar';
import { UiSwitchModule } from 'ngx-toggle-switch/src';


import { HomeComponent } from './home/home.component';
import { OrderService } from './services/order.service';
 
import { ViewOrderComponent } from './view-order/view-order.component';
import { ModalService } from './services/modal.service';
import { BsModalService, ComponentLoaderFactory, PositioningService, ModalBackdropComponent } from 'ngx-bootstrap';
import { ModalContainerComponent } from 'ngx-bootstrap/modal';
import { AlertService } from './services/alert.service';
import { ToastrService, TOAST_CONFIG, ToastrModule } from 'ngx-toastr';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [
    AppComponent, 
    ClearimageDirective,
    EditableDirective,
    InputCounterComponent,
    HomeComponent, 

    // ModalBackdropComponent,
    // ModalContainerComponent,

    ViewOrderComponent,
    ReplaceUnderscorePipe
    ],
  imports: [
    
    UiSwitchModule,
    BrowserModule,
    AppRoutingModule,
    TreeviewModule.forRoot(),
    AmazingTimePickerModule,
    ChartsModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
     
    SlimLoadingBarModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    TextMaskModule,
    DataTablesModule,
    SimpleNotificationsModule.forRoot(),
    NgxMyDatePickerModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
    ImageUploadModule.forRoot(),
    AngularMultiSelectModule,
    Ng2AutoCompleteModule,
    QuillEditorModule,
    Ng2ScrollableModule,
    FullCalendarModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    
    PositioningService,
    ComponentLoaderFactory,
    BsModalService,
    BsModalService,
    DatatableHelper,
    OrderService,
    ModalService,
    AlertService,
    ToastrService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },

  ],
  entryComponents:[
    ModalBackdropComponent,
    ModalContainerComponent,
    ViewOrderComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
