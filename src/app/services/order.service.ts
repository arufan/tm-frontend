import { Injectable,EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';


@Injectable()
export class OrderService {


  refreshList$ = new EventEmitter();

  constructor(private http:HttpClient) { 
    this.refreshList$ = new EventEmitter();
  }

  public create(body:any){
    return this.http.post( environment.base_url+"/orders", body).map( (resp:any) => {
      this.refreshList$.emit();
      return resp;
    }
    )
  }


  public getAll(){
    return this.http.get( environment.base_url+"/orders/list").map( (resp:any) => {
     
      return resp;
    }
  )
  }
  public get(id:number){
    return this.http.get( environment.base_url+"/orders/"+id).map( (resp:any) => {
     
      return resp;
    }
  )
  }

  public update(id:any,body:any){
    return this.http.put( environment.base_url+"/orders/"+id, body, { observe: 'response' }).map( (resp:any) => {
      this.refreshList$.emit();
      return resp;
    }
  )
  }

  public delete(id:any){
    return this.http.delete( environment.base_url+"/orders/"+id,  { observe: 'response' }).map( (resp:any) => {
      this.refreshList$.emit();
      return resp;
    }
  )
  }

}
