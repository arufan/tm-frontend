import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { ViewOrderComponent } from '../view-order/view-order.component';

@Injectable()
export class ModalService {

  selectedComponent : any;
  selectedConfig : any;
  modalState: any;
  modalRef: BsModalRef;

  configCustomMd: any = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: true,
    class: 'modal-md'
  };

  constructor(public modalService: BsModalService) { }


  openModel(modalView, _data){
    this.selectedComponent = ViewOrderComponent;
    this.selectedConfig = this.configCustomMd

    if(modalView=="view"){
      this.modalState = {
        title: 'View',
        data : _data,
      };
      this.selectedComponent = ViewOrderComponent;
    }else{
      this.modalState = {
        title: 'Save',
        data : _data,
      };
      this.selectedComponent = ViewOrderComponent;
    }

    this.selectedConfig.initialState = this.modalState;
    return this.modalRef = this.modalService.show(this.selectedComponent, this.selectedConfig);

  }

}
