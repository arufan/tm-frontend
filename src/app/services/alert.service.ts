import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';



@Injectable()
export class AlertService {

  constructor(public toastr: ToastrService) { }

  success(message: string, title: string) {
    console.log('cussess alrert called')
    this.toastr.success(title, message);
  }

  error(message: string, title: string) {
    this.toastr.error(title, message);
  }

  info(message: string, title: string) {
    this.toastr.info(title, message);
  }

  warn(message: string, title: string) {
    this.toastr.warning(title, message);
  }

}
