import { ImageUploadComponent } from 'angular2-image-upload';
import { Directive, Renderer, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[clearImage]'
})
export class ClearimageDirective {

  @Output() afterCleared = new EventEmitter<any>();
  constructor(private el: ImageUploadComponent, private renderer: Renderer) { }

  ngOnInit(){
    
  }

  @Input('clearWhen') set clearWhen(shouldClear: boolean) {
    if (shouldClear) {
      // If condition is true add template to DOM
      this.el.deleteAll();
      this.afterCleared.emit();
    } 
  }

}