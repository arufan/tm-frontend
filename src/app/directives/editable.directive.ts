import { FileHolder } from 'angular2-image-upload';
import { Directive, Renderer2, ElementRef, HostListener, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import * as $ from 'jquery';
@Directive({
  selector: '[editable]'
})
export class EditableDirective implements AfterViewInit {

  @Input() data: any;
  @Input() fieldKey: any;
  @Input() field: any;
  @Input() type: string;

  @Input() optionDisplay: any;
  @Input() optionValue: any;
  @Input() options: Array<any>;
  @Output() afterEdit = new EventEmitter();
  @Input() editMode:boolean=false;

  select:HTMLSelectElement;
  inputElement:HTMLElement;
  parent:any;
  constructor(private elem: ElementRef, private renderer: Renderer2) {
    this.parent = this.elem.nativeElement.parentNode;
  }

  @HostListener('click', ['$event']) onClick($event) {
    if(this.type=="select"){
      this.showSelect();
    }
    else
    {
      this.showInput();
      
    }
  }

  ngAfterViewInit() {
    
  }

  showSelect(){
    this.renderer.setStyle(this.elem.nativeElement,"display","none");
    this.renderer.setStyle(this.select,"display","block");
    //this.renderer.setAttribute(this.child,"focused","true");
    this.select.focus();
  }


  hideSelect(){

  }

  showInput(){
    this.renderer.setStyle(this.elem.nativeElement,"display","none");
    this.renderer.setStyle(this.inputElement,"display","block");
    //this.renderer.setAttribute(this.child,"focused","true");
    this.inputElement.focus();
  }

  ngOnChanges(changes: any) {
    if (changes['options'] && this.options) {
      if(this.type=="select"){
        //this.renderer.removeChild(this.parent,this.select);
        this.select = document.createElement('select');
        if(this.options!=undefined){
          for(let option of this.options){
            var opt=document.createElement('option');
            
            if(this.optionValue!=null)
              this.renderer.setAttribute(opt,"value",option[this.optionValue]);
            else
              this.renderer.setAttribute(opt,"value",option);

            if(this.field!=null){
              if(this.field[this.optionValue]==option[this.optionValue]) {
                this.renderer.setAttribute(opt,"selected","true");
              }
            }
            opt.innerHTML=option[this.optionDisplay];
            this.renderer.appendChild(this.select,opt);

          }

          
          this.renderer.insertBefore(this.parent,this.select,this.elem.nativeElement)
          this.renderer.setStyle(this.select,"display","none");
          this.renderer.addClass(this.select,"form-control");
        
          // this.renderer.listen(this.select, 'change', (event) => {
          //   this.onSelect(event);
            
          // })
          this.renderer.listen(this.select, 'blur', (event) => {
            this.onSelect(event);

          })
        }
        
      }
    }

    if (changes['type']) {
      if(this.type=="text"){
        this.inputElement = document.createElement('input');
        this.renderer.setAttribute(this.inputElement,"type","text");
        this.renderer.setAttribute(this.inputElement,"value",this.field);

        this.renderer.insertBefore(this.parent,this.inputElement,this.elem.nativeElement)
        this.renderer.setStyle(this.inputElement,"display","none");
        this.renderer.addClass(this.inputElement,"form-control");
      
        this.renderer.listen(this.inputElement, 'blur', (event) => {
          this.onEditDone(event);
        })
      }
    }
      
  }


  onSelect(event){
    this.field=this.options[event.target.selectedIndex];
    eval("this.data."+this.fieldKey+"=this.field");

    this.renderer.setStyle(this.elem.nativeElement,"display","block");
    this.renderer.setStyle(this.select,"display","none");
    this.afterEdit.emit({value:this.field, data:this.data});
  }

  onEditDone(event){
    this.field=event.target.value;
    eval("this.data."+this.fieldKey+"=this.field");
    this.renderer.setStyle(this.elem.nativeElement,"display","block");
    if(this.type=="select"){
      this.renderer.setStyle(this.select,"display","none");
    }
    else{
      this.renderer.setStyle(this.inputElement,"display","none");
    }
    
    this.afterEdit.emit({value:this.field, data:this.data});
  }


}
