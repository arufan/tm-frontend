import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { OrderService } from '../services/order.service';
import { AlertService } from '../services/alert.service';
import { DISABLED } from '@angular/forms/src/model';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css']
})
export class ViewOrderComponent implements OnInit {

  pageTitle:any="Add New Order";

  dateConfig: any;
  formData: any;
  order : any = {};
  submitTouched: boolean = false;
  orderForm : FormGroup
  title : string;
  mode : string;
  data : any;
  states:any=[
    'Johor',
    'Kedah',
    'Kelantan',
    'Negeri Sembilan',
    'Malacca',
    'Pahang',
    'Penang',
    'Perak',
    'Sabah',
    'Serawak'
  ];

  constructor(
    public modalRef : BsModalRef,
    private orderService:OrderService,
    private alertService:AlertService
    ) { 

    this.orderForm = new FormGroup({
      'order_id':new FormControl(null, [ Validators.required]),
      'service_number': new FormControl(null, [Validators.required]),
      'segment_group': new FormControl(null),
      'product_name': new FormControl(null),
      'remark': new FormControl(null, [Validators.required]),
      'status':new FormControl(null, [Validators.required]),
      'state':new FormControl(null, [Validators.required])
    });
  }

  ngOnInit() {
    console.log(this.data);
    this.mode=this.data.mode;

    this.orderForm.patchValue(this.data.data);
    this.order=this.data.data;

    if(this.mode==='edit'){
      this.pageTitle="Order Details";
    }
    
  }

  onSubmit(){
    console.log(this.orderForm.valid);
    if(this.orderForm.valid){
      var formData=this.orderForm.value;
      console.log(formData);
      ;
      if(this.mode=='edit'){
        this.orderService.update(this.order.id,formData).subscribe(res=>{
          console.log(res);
          this.alertService.success('Success', 'Order updated');
          this.modalRef.hide();
        });
      }else{
        this.orderService.create(formData).subscribe(res=>{
          console.log(res);
          this.alertService.success('Success', 'Order created');
          this.modalRef.hide();
        });
      }
    }else{
      for (var i in this.orderForm.controls) {
        this.orderForm.controls[i].markAsTouched();
      }
    }
    
    

  }
 
  onDelete(){
    console.log('delete called');
    if(confirm("Do you want to delete this order?")){
      this.orderService.delete(this.order.id).subscribe(res=>{
        this.alertService.success('Success', 'Order deleted');
        this.modalRef.hide();
      });
    }
  }

}
