
var data=[
    {
       "salesAgentProfile":{
          "id":1,
          "fullName":"sandeep raj gupta",
          "nickName":"sandy"
       },
       "totalGroupSalesValue":370.0,
       "agentHeirachyViewVMList":[
          {
             "salesAgentProfile":{
                "id":20,
                "fullName":"GM John",
                "nickName":"John"
                
             },
             "totalGroupSalesValue":370.0,
             "agentHeirachyViewVMList":[
                {
                   "salesAgentProfile":{
                      "id":34,
                      "fullName":"Sm Alice",
                      "nickName":null
                   },
                   "totalGroupSalesValue":0.0,
                   "agentHeirachyViewVMList":[
                      {
                         "salesAgentProfile":{
                            "id":36,
                            "fullName":"Tommy",
                            "nickName":"Tommy"
                           
                         },
                         "totalGroupSalesValue":0.0,
                         "agentHeirachyViewVMList":null
                      },
                      {
                         "salesAgentProfile":{
                            "id":37,
                            "fullName":"TM Carol",
                            "nickName":"Carol"
                         },
                         "totalGroupSalesValue":0.0,
                         "agentHeirachyViewVMList":[
                            {
                               "salesAgentProfile":{
                                  "id":38,
                                  "fullName":"SA Daniel",
                                  "nickName":"Daniel"
                                  
                               },
                               "totalGroupSalesValue":0.0,
                               "agentHeirachyViewVMList":[
                                  {
                                     "salesAgentProfile":{
                                        "id":39,
                                        "fullName":"SA Penny"
                                        
                                     },
                                     "totalGroupSalesValue":0.0,
                                     "agentHeirachyViewVMList":null
                                  }
                               ]
                            }
                         ]
                      }
                   ]
                },
                {
                   "salesAgentProfile":{
                      "id":35,
                      "fullName":"dash"
                      
                   },
                   "totalGroupSalesValue":0.0,
                   "agentHeirachyViewVMList":null
                }
             ]
          },
          {
             "salesAgentProfile":{
                "id":31,
                "fullName":"Habib"
             },
             "totalGroupSalesValue":0.0,
             "agentHeirachyViewVMList":null
          },
          {
             "salesAgentProfile":{
                "id":32,
                "fullName":"Arufan"
                
             }
          },
          {
             "salesAgentProfile":{
                "id":33,
                "fullName":"Hari"
                
             },
             "totalGroupSalesValue":0.0,
             "agentHeirachyViewVMList":null
          },
          {
             "salesAgentProfile":{
                "id":40,
                "fullName":"sandy raj"
                
             },
             "totalGroupSalesValue":0.0,
             "agentHeirachyViewVMList":null
          }
       ]
    }
 ];

 var table="<ul>";
 var lastLevel=0;
 generateTree(data, 0, 0);
 table+="</ul>";
 console.log(table);
 

 function generateTree(items,index, level){
    var space="";
        
    if(items[index]!=undefined && items[index]!=null){
        for(x=0; x<level; x++){
            space+="--";
        }
        //console.log(space+items[index].salesAgentProfile.fullName);
        table+="<li>"+items[index].salesAgentProfile.fullName+"</li>";
        if(items[index].agentHeirachyViewVMList!=null && items[index].agentHeirachyViewVMList.length>0){
            table+="<ul>";
            generateTree(items[index].agentHeirachyViewVMList, 0, level+1) ;
            table+="</ul>";
        }
    }

    if(index<items.length){
        generateTree(items,index+1, level);
    }

   
    lastLevel=level;
    
    
 }

 //console.log(JSON.stringify(tree));