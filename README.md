Introduction
============

This is the **Angular** version of **AdminLTE** -- what is a fully responsive admin template. Based on **[Bootstrap 3](https://github.com/twbs/bootstrap)** framework. Highly customizable and easy to use. Fits many screen resolutions from small mobile devices to large desktops. Check out the live preview now and see for yourself.

For more AdminLTE information visit  [AdminLTE.IO](https://adminlte.io/)

Installation
------------

- Fork the repository ([here is the guide](https://help.github.com/articles/fork-a-repo/)).
- Clone to your machine
- Install Angular 2 Client.
```bash
npm install -g @angular/cli
```
- Clone the repository
```bash
git clone https://github.com/YOUR_USERNAME/Angular2-AdminLTE.git
```

- Install the packages
```bash
cd Angular2-AdminLTE
npm install
```

Running the application
------------
- On the folder project
```
ng serve
```
- For starter page Navigate to [http://localhost:4200/](http://localhost:4200/)
- For admin page Navigate to [http://localhost:4200/admin](http://localhost:4200/admin)

Browser Support
---------------
- IE 9+
- Firefox (latest)
- Chrome (latest)
- Safari (latest)
- Opera (latest)

Contribution
------------
Contribution are always **welcome and recommended**! Here is how:

- Fork the repository ([here is the guide](https://help.github.com/articles/fork-a-repo/)).
- Clone to your machine ```git clone https://github.com/YOUR_USERNAME/Angular2-AdminLTE.git```
- Make your changes
- Create a pull request

#### Contribution Requirements:
- Contributions are only accepted through Github pull requests.
- Finally, contributed code must work in all supported browsers (see above for browser support).

License
-------
Angular2-AdminLTE is an open source project by that is licensed under [MIT](http://opensource.org/licenses/MIT).

 Credits
-------------
[AdminLTE.IO](https://adminlte.io/)


token
======
eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdXBlcl9hZG1pbjQiLCJhdXRoIjoiUk9MRV9TVVBFUl9BRE1JTiIsImV4cCI6MTUxNDk0NDY1OH0.xs6QuypQjNwDiXeHbn7psZ75aceHcicZpjOhzjONmn6vNMLuJURnmPqGcQ3tF8TK3ix5MDTUb5LX1oklyMkOjQ

sagarmac.local
=========
eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0lUX0FETUlOIiwiZXhwIjoxNTEyNzA2Njk5fQ.TS9RtlPnyf4bjHnIPGlywW2Xdm1JKIYMf8-ckcpun8qohrLl2EeOWF6BoXUJi6FNBnyRGQ-ULZ8H24eMzHp1BQ