FROM node:7
WORKDIR /app
COPY package.json /app
RUN npm install
RUN npm install -g @angular/cli
COPY . /app


CMD node server.js
EXPOSE 8343