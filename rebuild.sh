#!/bin/bash
imageName=ioncareweb:0.1
containerName=eaf76efe69f7

docker build -t $imageName -f Dockerfile  .

echo Delete old container...
docker rm -f $containerName

echo Run new container...
docker run -d -p 8343:8343 --name $containerName $imageName